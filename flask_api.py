from flask import Flask, request
# from flask import jsonify
# from flask import request
from flask_cache import Cache 
# from flask_restless import APIManager
# from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table
from sqlalchemy import select
from sqlalchemy.orm import sessionmaker

import json


def setup_db():
    db = create_engine('sqlite:///db_airports_details_oth.db')
    db.echo = False
    metadata = MetaData(db)
    airports_details_table = Table('airports_details_oth', metadata, autoload=True)

    return (db, airports_details_table)

db, airports_details_table = setup_db()
app = Flask(__name__)

session = sessionmaker()
session.configure(bind=db)
app.session = session()
cache = Cache(app, config={'CACHE_TYPE': 'redis'})


def make_cache_key(*args, **kwargs):
    path = request.path
    args = str(hash(frozenset(request.args.items())))
    return (path + args)


@app.route('/airports_details/', methods = ['GET'])
@cache.cached(timeout=50*100, key_prefix=make_cache_key)
def index():
    select_stmt = select([airports_details_table.c.Name, airports_details_table.c.IATA, airports_details_table.c.ICAO,
                        airports_details_table.c.City, airports_details_table.c.Country, airports_details_table.c.Latitude,
                        airports_details_table.c.Longitude])
    result = app.session.execute(select_stmt)
    json_res = json.dumps( [dict(ix) for ix in result], indent=4, sort_keys=True)
    
    return json_res

@app.route('/iata_search', methods = ['POST'])
@cache.cached(timeout=50*100, key_prefix=make_cache_key)
def get_iata_details():
    arg_val = request.args.get('iata').upper()
    search_term = arg_val+"%"
    select_stmt = select([airports_details_table.c.Name, airports_details_table.c.IATA, airports_details_table.c.ICAO,
                        airports_details_table.c.City, airports_details_table.c.Country, airports_details_table.c.Latitude,
                        airports_details_table.c.Longitude]).where(airports_details_table.c.IATA.like(search_term))
    result = app.session.execute(select_stmt)
    result_list = [dict(ix) for ix in result]
    
    if len(result_list) == 1:
        json_res = json.dumps( result_list[0], indent=4, sort_keys=True)
    else:
        json_res = json.dumps( result_list, indent=4, sort_keys=True)

    return json_res


@app.route('/airport_search', methods = ['POST'])
@cache.cached(timeout=50*100, key_prefix=make_cache_key)
def get_airport_details():
    arg_val = request.args.get('name').lower()
    search_term = "%"+arg_val+"%"
    select_stmt = select([airports_details_table.c.Name, airports_details_table.c.IATA, airports_details_table.c.ICAO,
                        airports_details_table.c.City, airports_details_table.c.Country, airports_details_table.c.Latitude,
                        airports_details_table.c.Longitude]).where(airports_details_table.c.Name.like(search_term))
    result = app.session.execute(select_stmt)
    result_list = [dict(ix) for ix in result]
    if len(result_list) == 1:
        json_res = json.dumps( result_list[0], indent=4, sort_keys=True)
    else:
        json_res = json.dumps( result_list, indent=4, sort_keys=True)

    return json_res


if __name__ == '__main__':
    app.run(port=5555)

appnope==0.1.0
click==6.7
decorator==4.0.11
Flask==0.12.2
Flask-Cache==0.13.1
Flask-Restless==0.17.0
Flask-SQLAlchemy==2.2
ipdb==0.10.3
ipython
ipython-genutils==0.2.0
itsdangerous==0.24
jedi==0.10.2
Jinja2==2.9.6
MarkupSafe==1.0
mimerender==0.6.0
numpy==1.13.0
pandas==0.20.2
pexpect==4.2.1
pickleshare==0.7.4
prompt-toolkit==1.0.14
ptyprocess==0.5.1
Pygments==2.2.0
python-dateutil==2.6.0
python-mimeparse==1.6.0
pytz==2017.2
redis==2.10.5
simplegeneric==0.8.1
six==1.10.0
SQLAlchemy==1.1.11
traitlets==4.3.2
virtualenv==15.1.0
wcwidth==0.1.7
Werkzeug==0.12.2
import pandas as pd
import sqlite3
import redis

from settings import REDIS_SERVER
from settings import REDIS_SERVER_IP
from settings import REDIS_SERVER_PORT


conn = sqlite3.connect('db_airports_details_oth.db')
conn.text_factory = str
c = conn.cursor()
col_name = ['Airport_ID', 'Name', 'City', 'Country', 'IATA', 'ICAO', 'Latitude', 'Longitude', 'Altitude', 'Timezone', 'DST', 'Tz', 'Type', 'Source']
# change to url of csv file
# csvfile = 'airports_other.csv'
csvfile = 'https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat'
df = pd.read_csv(csvfile, names= col_name)


conn.execute("""drop table if exists airports_details_oth""")
conn.execute("""create table airports_details_oth(
        Airport_ID     int     primary key not NULL ,
        Name  text,
        City   text,
        Country    text,
        IATA    text,
        ICAO   text,
        Latitude    text,
        Longitude   text,
        Altitude   text, 
        Timezone    text,
        DST    text,
        Tz   text,
        Type    text,
        Source   text
        )""")
df.to_sql('airports_details_oth', conn, if_exists='append', index=False)

# testing to check that data inserted
stmt = """delete from airports_details_oth where airports_details_oth.iata is Null OR trim(IATA) = '\\N' """
result = conn.execute(stmt)
conn.commit()

print("Inserted data from csv url to sqlite db")

conn.close()

# deleting all the cache keys after updating all database
if REDIS_SERVER:
    r = redis.StrictRedis(host=REDIS_SERVER_IP, port=REDIS_SERVER_PORT, db=0)
    for key in r.scan_iter("flask_cache_*"):
        r.delete(key)
    print("deleted cache from Redis server")    

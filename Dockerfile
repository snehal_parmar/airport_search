FROM debian
RUN apt-get update -y && \
    apt-get install -y python-pip python-dev && \
    apt-get install -y curl && \
    apt-get install -y redis-server
# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
#RUN redis-server --daemonize yes
COPY . /app

ENTRYPOINT [ "python" ]
CMD [ "flask_api.py" ]

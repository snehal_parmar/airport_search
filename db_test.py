from sqlalchemy import *
import json


db = create_engine('sqlite:///db_airports_details_oth.db')

db.echo = False

metadata = MetaData(db)

airports_details_oth = Table('airports_details_oth', metadata, autoload=True)

# import ipdb; ipdb.set_trace()

select_stmt = select([airports_details_oth.c.Name, airports_details_oth.c.IATA, airports_details_oth.c.ICAO,
                        airports_details_oth.c.City, airports_details_oth.c.Country, airports_details_oth.c.Latitude,
                        airports_details_oth.c.Longitude])

result = db.execute(select_stmt)
json_res = json.dumps( [dict(ix) for ix in result], indent=4, sort_keys=True)

print(json_res)
